/* Utility to calculate hashes for lexemes
 *
 *  calc_hashes.c
 *
 *  Copyright (C) 2008, Benjamin Kowarsch.  All rights reserved.
 *
 *  License:
 *
 *  Redistribution  and  use  in source  and  binary forms,  with  or  without
 *  modification, are permitted provided that the following conditions are met
 *
 *  1) NO FEES may be charged for the provision of the software.  The software
 *     may  NOT  be published  on websites  that contain  advertising,  unless
 *     specific  prior  written  permission has been obtained.
 *
 *  2) Redistributions  of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *  3) Redistributions  in binary form  must  reproduce  the  above  copyright
 *     notice,  this list of conditions  and  the following disclaimer  in the
 *     documentation and other materials provided with the distribution.
 *
 *  4) Neither the author's name nor the names of any contributors may be used
 *     to endorse  or  promote  products  derived  from this software  without
 *     specific prior written permission.
 *
 *  5) Where this list of conditions  or  the following disclaimer, in part or
 *     as a whole is overruled  or  nullified by applicable law, no permission
 *     is granted to use the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,  BUT NOT LIMITED TO,  THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY  AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT  SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE  FOR  ANY  DIRECT,  INDIRECT,  INCIDENTAL,  SPECIAL,  EXEMPLARY,  OR
 * CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE,  DATA,  OR PROFITS; OR BUSINESS
 * INTERRUPTION)  HOWEVER  CAUSED  AND ON ANY THEORY OF LIABILITY,  WHETHER IN
 * CONTRACT,  STRICT LIABILITY,  OR TORT  (INCLUDING NEGLIGENCE  OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,  EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *  
 */


// ---------------------------------------------------------------------------
// imports
// ---------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include "hash.h"


// ---------------------------------------------------------------------------
// function:  main(argc, argv)
// ---------------------------------------------------------------------------
//
// Reads one or more arguments from the command line, interprets each argument
// as a lexeme,  then calculates a hash value for each lexeme   and prints the
// the calculated hash value as a sedecimal integer to the console (stdout).

int main (int argc, const char **argv) {
    unsigned int nth_arg, index, hash;
    unsigned char *lex;
	
    if (argc <= 1) {
        printf("usage: calc_hashes string { string }\n");
        exit(1);
    } // end if

    for (nth_arg = 1; nth_arg < argc; nth_arg++) {
    
        lex = (unsigned char *)argv[nth_arg];
                
        index = 0;
        hash = HASH_INITIAL;
        
        while (lex[index] != 0) {
            hash =  HASH_NEXT_CHAR(hash, lex[index]);
            index++;
        } // end while
        
        hash = HASH_FINAL(hash);

        printf("0x%8X   /* %s */\n", hash, lex);

    } // end for
    
    return 0;
} // end main


// END OF FILE
